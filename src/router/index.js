import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import Summoner from '@/components/Summoner'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/summoner/:id',
      name: 'Summoner',
      component: Summoner
    }
  ]
})
