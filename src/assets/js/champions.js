import axios from 'axios'

export function getAllChampions () {
  axios
    .get('http://ddragon.leagueoflegends.com/cdn/6.24.1/data/en_US/champion.json')
    .then(response => {
      this.allChampions = response.data
      console.log(this.allChampions)
    })
    .catch(e => {
      console.log(e)
    })
}
