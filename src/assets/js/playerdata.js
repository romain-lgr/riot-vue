import axios from 'axios'

const proxy = 'https://cors-anywhere.herokuapp.com/'
const ApiKey = process.env.API_KEY
const riotBaseUrl = 'https://euw1.api.riotgames.com/lol/'

export function getPlayerData (summoner) {
  // FIRST GET ALL CHAMPIONS
  let allChampions = []
  let champions = []
  axios
  .get('http://ddragon.leagueoflegends.com/cdn/6.24.1/data/en_US/champion.json')
  .then(response => {
    allChampions = response.data.data    
  })
  .catch(e => {
    console.log(e)
  })
  // GET INFO BY SUMMONER NAME
  const byNameUrl = proxy + riotBaseUrl + 'summoner/v3/summoners/by-name/' + summoner + '?api_key=' +
    ApiKey
  // AXIOS NESTED CALLS
  // SUMMONER NAME AXIOS CALL
  axios
    .get(byNameUrl)
    .then(response => {
      this.resultsByName = response.data
      this.accountId = this.resultsByName.accountId
      const bySummonerUrl = proxy + riotBaseUrl + 'league/v3/positions/by-summoner/' + this.resultsByName.id +
        '?api_key=' + ApiKey
      const masteryUrl = proxy + riotBaseUrl + 'champion-mastery/v3/champion-masteries/by-summoner/' + this.resultsByName.id +
        '?api_key=' + ApiKey
      // SUMMONER ID AXIOS CALL
      axios
        .get(bySummonerUrl)
        .then(response => {
          this.resultsById = response.data[0]
          this.showData = true
        })
        .catch(e => {
          console.log(e)
        })
        // SUMMONER MASTERY AXIOS CALL
      axios
        .get(masteryUrl)
        .then(response => {
          this.resultsMastery = response.data.slice(-5)
          this.showMastery = true
          for (const key in allChampions)
          allChampions[key] = allChampions[key]
          // for (const championId in this.resultsMastery) {
          //   champions[championId] = this.resultsMastery[championId]
          // }
          for(var key in this.resultsMastery) 
          champions[key] = this.resultsMastery[key]
          console.log(champions[key].championId)
          // this.resultsMastery.forEach(function (champ) {
          //   champions[championId] = champ.championId
          // })
          if (allChampions[key] === champions[key]) {
            console.log('')
          }
          this.showMastery = true
        })
        .catch(e => {
          console.log(e)
        })
    })
    .catch(e => {
      console.log(e)
    })
}
